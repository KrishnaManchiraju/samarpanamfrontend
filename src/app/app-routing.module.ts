import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './login/auth.guard';
import { LoginComponent } from './login/login.component';
import { ReqDetailComponent } from './req-detail/req-detail.component';
import { ReqlistComponent } from './reqlist/reqlist.component';
import { RequestComponent } from './request/request.component';
import { RudrakshaComponent } from './rudraksha/rudraksha.component';
import { VerifyComponent } from './verify/verify.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'verify', component: VerifyComponent },
  { path: 'detail/:id', component: ReqDetailComponent, canActivate: [AuthGuard] },
  { path: 'request', component: RequestComponent, canActivate: [AuthGuard] },
  { path: 'request-list', component: ReqlistComponent, canActivate: [AuthGuard] },
  { path: 'landing', component: ReqlistComponent, canActivate: [AuthGuard] },
  { path: 'rudraksha', component: RudrakshaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
