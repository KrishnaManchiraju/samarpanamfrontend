export class RudrakshaRecord {
    reg_id: string;
    first_name: string;
    last_name: string;
    email: string;
    country: string;
    street: string;
    house_number: string;
    city: string;
    postcode: string;
}

export enum StatusCodes {
    OK = 200,
    FOUND = 202,
    LOCKED = 423,
    BAD_REQUEST = 400,
    NOT_FOUND = 404
}