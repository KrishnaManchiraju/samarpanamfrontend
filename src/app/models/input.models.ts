export class LoginCredentials {
    email: string;
    password: string;
}

export class RegistrationDetails extends LoginCredentials {
    firstName: string;
    lastName: string;
}

export class EmailRequest {
    id?: string;
    email_subject: string;
    from_address: string;
    audience: number[];
    reviewers: number[];
    expected_delivery: string;
    details: string;
    created_by?: string;
    modified_by?: string;
    email_type?: number;
    status?: string;
    template?: File;
    email_provider?: string;
    email_provider_request_ref?: string;
}

export class EmailRequestCompletion {
    id: string;
    email_provider?: string;
    email_provider_request_ref?: string;
}

export class EmailProvider {
    email_provider: string;
    email_provider_desc: string;
}