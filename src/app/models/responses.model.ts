export class AuthResponse {
    access: string;
    refresh: string;
}

export class Audience {
    id: number;
    name: string;
}

export class Reviewer {
    id: number;
    email: string;
    name: string;
}

export class Review {
    id: number;
    reviewer: Reviewer;
    approved: Boolean;
    approval_time: string;
}

export class EmailType {
    id: number;
    email_type: string;
}

export class Status {
    status: string;
    style: string;
    step: number;
}

export class PaginatedPartialEmailRequestResponse{
    next: string;
    previous: string;
    results: PartialEmailRequestResponse[]
}

export class PartialEmailRequestResponse {
    id: number;
    email_subject: string;
    expected_delivery: string;
    created_by: string;
    status: Status;
    email_type: string;
}

export class DetailedEmailRequestResponse {
    id: number;
    email_subject: string;
    expected_delivery: string;
    created_by: string;
    created_at: string;
    audience: Audience[];
    reviews: Review[];
    from_address: string;
    details: string;
    email_type: EmailType;
    status: Status;
    template: any;    
    email_provider: string;
    email_provider_desc: string;
    email_provider_request_ref: string;
}

export class EmailRequestCompletionResponse {
    id: number;
    status: Status;
    email_provider: string;
    email_provider_desc: string;
    email_provider_request_ref: string;
}

export class EmailProviderChoicesResponse {
    email_provider: string;
    email_provider_desc: string;
}

export class EmailStatsResponse {
    successul_deliveries: number;
    unique_opens: number;
    unique_clicks: number;
    total_clicks: number;
    abuse_reports: number;
    unsubscribed: number;
    sent_at: string;
}