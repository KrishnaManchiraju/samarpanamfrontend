import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IAngularMyDpOptions, IMyDateModel } from 'angular-mydatepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { BackendService } from '../backend.service';
import { AuthService } from '../login/auth.service';
import { Audience, DetailedEmailRequestResponse, EmailType, Reviewer, Status } from '../models/responses.model';
import { EmailRequest } from '../models/input.models';
import { ToastService } from '../toasts/toasts.service';

enum ViewType {
  Create = 1,
  Observe,
  Edit
}

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {
  ViewType = ViewType;
  public m_requestForm: FormGroup;
  public m_datePickerOptions: IAngularMyDpOptions;
  public m_audienceDropdownList = [];
  public m_audienceSelectedItems = [];
  public m_audienceDropdownSettings = {};
  public m_reviwersDropdownList = [];
  public m_reviwersSelectedItems = [];
  public m_reviwersDropdownSettings = {};
  public m_emailTypes: EmailType[];
  public m_statusTypes: Status[];
  public m_viewType: ViewType;
  public m_reqId: string;
  public m_isDateSelected: boolean;
  public m_template: File;
  public m_templateUrl: string;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
    private backend: BackendService,
    private toast: ToastService,
    private spinner: NgxSpinnerService) {

    this.initializeForm();
  }

  ngOnInit(): void {
    this.populateDropdowns();

    const observe = this.route.snapshot.queryParamMap.get("observe");
    const edit = this.route.snapshot.queryParamMap.get("edit");

    if (!!observe) {
      this.m_viewType = ViewType.Observe;
      this.m_reqId = observe;
      // this.m_requestForm.disable();
    } else if (!!edit) {
      this.m_viewType = ViewType.Edit;
      this.m_reqId = edit;
    } else {
      this.m_viewType = ViewType.Create;
    }

    if (!!observe || !!edit) {
      this.backend.getDetailedRequest(this.m_reqId).subscribe(
        (res: DetailedEmailRequestResponse) => {
          this.m_requestForm.controls.email_subject.setValue(res.email_subject);
          this.m_requestForm.controls.from_address.setValue(res.from_address);
          this.m_audienceSelectedItems = res.audience;
          this.m_templateUrl = res.template;  
          //this.m_template = res.template;     
          const splitDate = res.expected_delivery.split('.');
          const date_model: IMyDateModel = {
            isRange: false, singleDate: {
              formatted: res.expected_delivery,
              date: {
                year: +splitDate[2],
                month: +splitDate[1],
                day: +splitDate[0]
              }
            }, dateRange: null
          };
          this.m_requestForm.patchValue({ expected_delivery: date_model });
          this.m_reviwersSelectedItems = [];
          for (let review of res.reviews) {
            this.m_reviwersSelectedItems.push(review.reviewer);
          }
          this.m_requestForm.controls.details.setValue(res.details);
          this.m_requestForm.controls.status.setValue(res.status.status);
          if (res.email_type) {
            this.m_requestForm.controls.email_type.setValue(res.email_type.id);
          }
          this.m_requestForm.controls.email_provider.setValue(res.email_provider);
          this.m_requestForm.controls.email_provider_request_ref.setValue(res.email_provider_request_ref);
        },
        (err: any) => { },
        () => { }
      );
    }
  }

  public submitRequest() {

    const isValid = this.validateForm();

    if (isValid) {      
      const reqDetails: EmailRequest = Object.assign({}, this.m_requestForm.value);
      reqDetails.audience = this.m_audienceSelectedItems.map(a => a.id);
      reqDetails.reviewers = this.m_reviwersSelectedItems.map(a => a.id);
      reqDetails.expected_delivery = this.m_requestForm.value.expected_delivery.singleDate.formatted;
      reqDetails.created_by = this.auth.userId;
      reqDetails.template = this.m_template;      
      this.spinner.show();
      this.backend.saveRequest(reqDetails).subscribe(
        (res: any) => {
          this.toast.toastSuccess("Email Request Submitted");
          this.router.navigateByUrl('/landing');
        },
        (err: any) => {
          this.toast.toastError("Could not submit the request. Please contact the emailing team");
          this.spinner.hide();
        },
        () => { this.spinner.hide(); }
      );
    }
  }

  public updateRequest() {
    const isValid = this.validateForm();

    if (isValid) {
      const reqDetails: EmailRequest = Object.assign({}, this.m_requestForm.value);
      reqDetails.id = this.m_reqId;
      reqDetails.audience = this.m_audienceSelectedItems.map(a => a.id);
      reqDetails.reviewers = this.m_reviwersSelectedItems.map(a => a.id);
      reqDetails.expected_delivery = this.m_requestForm.value.expected_delivery.singleDate.formatted;
      reqDetails.modified_by = this.auth.userId;    
      reqDetails.template = this.m_template;       
      this.spinner.show();
      this.backend.updateRequest(reqDetails).subscribe(
        (res: any) => {
          this.toast.toastSuccess("Email Request Updated");
          this.router.navigateByUrl('/landing');
        },
        (err: any) => {
          this.toast.toastError("Could not update the request. Please contact the emailing team");
          this.spinner.hide();
        },
        () => {
          this.spinner.hide();
          const url = '/detail/' + this.m_reqId;
          this.router.navigateByUrl(url);
        });
    }
  }

  private initializeForm() {
    this.m_requestForm = this.formBuilder.group({
      email_subject: [null, Validators.required],
      from_address: ["uk@ishafoundation.org", [Validators.required, Validators.email]],
      expected_delivery: [null, Validators.required],
      details: [null],
      email_type: [null],
      status: [null],
      template: [''],
      email_provider: [null] ,
      email_provider_request_ref: [null] 
    });

    this.initDatePicker();
    this.initAudienceDropdown();
    this.initReviewersDropdown();
  }

  private initAudienceDropdown() {
    this.m_audienceDropdownSettings = {
      singleSelection: false,
      text: "-",
      labelKey: "name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true
    };
  }

  private initReviewersDropdown() {
    this.m_reviwersDropdownSettings = {
      singleSelection: false,
      text: "Who's it now?",
      labelKey: "name",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true
    };
  }

  private initDatePicker() {
    const today = new Date();
    this.m_datePickerOptions = {
      dateFormat: 'dd.mm.yyyy',
      sunHighlight: false,
      disableUntil: {
        year: today.getFullYear(),
        month: today.getMonth() + 1,
        day: today.getDate()
      },
      disableDates: [
        { year: 2020, month: 12, day: 14 },
        { year: 2020, month: 12, day: 19 }
      ],
      highlightDates: [
        { year: 2020, month: 12, day: 14 },
        { year: 2020, month: 12, day: 19 },
        { year: 2020, month: 12, day: 22 }
      ]
    };
  }

  onDateChanged(event: IMyDateModel) {
    this.m_isDateSelected = !!event.singleDate.epoc;
  }

  fileChange (event: any) {
    if(event.target.files.length >0)
    {
      this.m_template = event.target.files[0] ;      
    }
  }

  private populateDropdowns() {
    this.backend.getAudienceList().subscribe(
      (res: Audience[]) => { this.m_audienceDropdownList = res; },
      (err: any) => { },
      () => { }
    );

    this.backend.getReviewersList().subscribe(
      (res: Reviewer[]) => { this.m_reviwersDropdownList = res; },
      (err: any) => { },
      () => { }
    );

    if (this.auth.isAdmin) {
      this.backend.getEmailTypesList().subscribe(
        (res: EmailType[]) => { this.m_emailTypes = res; },
        (err: any) => { },
        () => { }
      );
      this.backend.getReqStatusList().subscribe(
        (res: Status[]) => { this.m_statusTypes = res; },
        (err: any) => { },
        () => { }
      );
    }
  }

  private validateForm(): boolean {
    let isValid = true;

    if (!this.m_requestForm.controls.email_subject.valid) {
      this.toast.toastError("Subject cannot be empty");
      isValid = false;
    }
    if (!this.m_requestForm.controls.from_address.valid) {
      this.toast.toastError("Invalid email (from) address");
      isValid = false;
    }

    if (!this.m_requestForm.value.expected_delivery) {
      this.toast.toastError("Please specify a delivery date");
      isValid = false;
    }

    if(this.m_template && ((!this.m_template.name.toLowerCase().endsWith("html")) && (!this.m_template.name.toLowerCase().endsWith("htm"))) )
    {      
      this.toast.toastError("Please upload a html/htm file");
      isValid = false;
    }

    if (!isValid) {
      this.toast.toastError("Request Incomplete. All the fields marked with * are mandatory");
    }

    if (isValid && !this.m_audienceSelectedItems) {
      this.toast.toastError("Audience list cannot be empty");
      isValid = false;
    }

    if (isValid && !this.m_reviwersSelectedItems) {
      this.toast.toastError("Reviwers list cannot be empty");
      isValid = false;
    }

    return isValid;
  }

}
