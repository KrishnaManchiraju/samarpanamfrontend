import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { EmailRequest, EmailRequestCompletion } from './models/input.models';
import {
  Audience,
  DetailedEmailRequestResponse,
  EmailType,
  PaginatedPartialEmailRequestResponse,
  Reviewer,
  Status,
  EmailRequestCompletionResponse,
  EmailProviderChoicesResponse,
  EmailStatsResponse
} from './models/responses.model';
import { RudrakshaRecord } from './models/rudraksha.model';



@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private rootUrl: string;

  constructor(private http: HttpClient) {
    if (environment.production) {
      this.rootUrl = '';
    } else {
      this.rootUrl = 'http://127.0.0.1:8000/';
    }
  }

  public getRequestList(page: number = 1, page_size: number = 10) {
    const endPoint = 'api-request/list/';
    const params = new HttpParams()
      .set('page', page.toString())
      .set('page_size', page_size.toString());

    return this.http.get<PaginatedPartialEmailRequestResponse>(this.rootUrl + endPoint, { params: params });
  }

  public getDetailedRequest(id: string): Observable<DetailedEmailRequestResponse> {
    const endPoint = 'api-request/detail/';
    const params = new HttpParams()
      .set('id', id);

    return this.http.get<DetailedEmailRequestResponse>(this.rootUrl + endPoint, { params: params });
  }

  public getAudienceList(): Observable<Audience[]> {
    const endPoint = 'api-request/audience/';

    return this.http.get<Audience[]>(this.rootUrl + endPoint);
  }

  public getReviewersList(): Observable<Reviewer[]> {
    const endPoint = 'api-request/reviewers/';

    return this.http.get<Reviewer[]>(this.rootUrl + endPoint);
  }

  public getEmailTypesList(): Observable<EmailType[]> {
    const endPoint = 'api-request/types/';

    return this.http.get<EmailType[]>(this.rootUrl + endPoint);
  }

  public getReqStatusList(): Observable<Status[]> {
    const endPoint = 'api-request/status/';

    return this.http.get<Status[]>(this.rootUrl + endPoint);
  }

  public saveRequest(req: EmailRequest) {
    const endPoint = 'api-request/create/';
    let requestForm = this.convertToFormData(req);

    return this.http.post(this.rootUrl + endPoint, requestForm);
  }

  public updateRequest(req: EmailRequest) {
    const endPoint = 'api-request/update/';
    let updateForm = this.convertToFormData(req);

    return this.http.post(this.rootUrl + endPoint, updateForm);
  }

  public updateRequestStatus(id: number, step: number): Observable<Status> {
    const endPoint = 'api-request/update_status/';

    return this.http.put<Status>(this.rootUrl + endPoint, { id, step });
  }

  public approveRequestReview(review_id: number) {
    const endPoint = 'api-request/approve/';

    return this.http.put(this.rootUrl + endPoint, { review_id });
  }

  public completeRequest(req: EmailRequestCompletion): Observable<EmailRequestCompletionResponse>  {
    const endPoint = 'api-request/complete/';

    return this.http.put<EmailRequestCompletionResponse>(this.rootUrl + endPoint, req);
  }

  public getEmailProviders(): Observable<EmailProviderChoicesResponse[]>  {
    const endPoint = 'api-request/emailproviders/';

    return this.http.get<EmailProviderChoicesResponse[]>(this.rootUrl + endPoint);
  }

  public getEmailStats(id: number): Observable<EmailStatsResponse>  {
    const endPoint = 'api-request/emailstats/';
    const params = new HttpParams()
      .set('id', id.toString());

    return this.http.get<EmailStatsResponse>(this.rootUrl + endPoint, { params: params });
  }  
  public getRudrakshaRecord(id: string, email: string): Observable<RudrakshaRecord> {
    const endPoint = 'api-rd/get_record/';
    const params = new HttpParams()
      .set('id', id)
      .set('email', email);

    return this.http.get<RudrakshaRecord>(this.rootUrl + endPoint, { params: params });
  }

  public updateRudrakshaRecord(rd_obj: RudrakshaRecord) {
    const endPoint = 'api-rd/update/';

    return this.http.put(this.rootUrl + endPoint, rd_obj);
  }

  private convertToFormData(req: EmailRequest): FormData {
    let fd = new FormData();

    Object.keys(req).forEach(key => {
      if (req[key]) {
        if (req[key] instanceof Array) {
          for (let val of req[key]) {
            fd.append(key, val);
          }
        }
        else {
          fd.append(key, req[key]);
        }
      }
    });

    return fd;
  }

}
