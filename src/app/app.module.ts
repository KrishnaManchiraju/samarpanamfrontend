import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RequestComponent } from './request/request.component';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { ToastsComponent } from './toasts/toasts.component';
import { NavbarComponent } from './navbar/navbar.component';
import { VerifyComponent } from './verify/verify.component';
import { ReqlistComponent } from './reqlist/reqlist.component';
import { AuthInterceptor } from './login/auth.interceptor';
import { ReqDetailComponent } from './req-detail/req-detail.component';
import { RudrakshaComponent } from './rudraksha/rudraksha.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RequestComponent,
    ToastsComponent,
    NavbarComponent,
    VerifyComponent,
    ReqlistComponent,
    ReqDetailComponent,
    RudrakshaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMyDatePickerModule,
    AngularMultiSelectModule,
    NgxSpinnerModule,
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrftoken',
      headerName: 'X-CSRFToken',
    })
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
