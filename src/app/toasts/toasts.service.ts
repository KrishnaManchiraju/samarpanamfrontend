import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
    toasts: any[] = [];

    toastError(message: string) {
        this.show(message, { classname: 'badge-danger text-bold' });
    }

    toastSuccess(message: string) {
        this.show(message, { classname: 'bg-success text-bold' });
    }

    show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
        this.toasts.push({ textOrTpl, ...options });
    }

    remove(toast) {
        this.toasts = this.toasts.filter(t => t !== toast);
    }
}
