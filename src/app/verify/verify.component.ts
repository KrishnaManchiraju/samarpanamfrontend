import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ToastService } from '../toasts/toasts.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService,
    private toastService: ToastService,
    private spinner: NgxSpinnerService,) { }

  ngOnInit(): void {
    this.spinner.show();
    const email = this.route.snapshot.queryParamMap.get("email");
    const code = this.route.snapshot.queryParamMap.get("code");

    this.auth.activateAccount(email, code).subscribe(
      (res: any) => {
        this.spinner.hide();
        this.toastService
          .toastSuccess(
            "Account Successfully Activated");
        this.router.navigateByUrl('/request-list');
      },
      (err: HttpErrorResponse) => {
        this.spinner.hide();
        const errMsg = (err.error && err.error.message) ? err.error.message : err.message;
        this.toastService.toastError(errMsg);
      },
      () => { }
    )
  }

}
