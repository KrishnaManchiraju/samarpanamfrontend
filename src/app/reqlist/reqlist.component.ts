import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { BackendService } from '../backend.service';
import { PaginatedPartialEmailRequestResponse } from '../models/responses.model';
import { ToastService } from '../toasts/toasts.service';

@Component({
  selector: 'app-reqlist',
  templateUrl: './reqlist.component.html',
  styleUrls: ['./reqlist.component.scss']
})
export class ReqlistComponent implements OnInit {
  public m_requestsList: PaginatedPartialEmailRequestResponse;

  constructor(private backend: BackendService,
    private toast: ToastService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    //load first page
    this.getRequestListByPage();
  }

  public getRequestListByPage(page: number = 1) {
    this.spinner.show();
    this.backend.getRequestList(page).subscribe(
      (res: PaginatedPartialEmailRequestResponse) => {
        let concatResults;
        if (this.m_requestsList) {
          concatResults = this.m_requestsList.results.concat(res.results);
        }
        this.m_requestsList = res;
        this.m_requestsList.results = concatResults || res.results;
      },
      (err: any) => {
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      }
    );
  }

  public sayIncubating() {
    this.toast.toastError("Filtering is an incubating feature");
  }

}
