import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BackendService } from '../backend.service';
import { RudrakshaRecord, StatusCodes } from '../models/rudraksha.model';
import { ToastService } from '../toasts/toasts.service';

@Component({
  selector: 'app-rudraksha',
  templateUrl: './rudraksha.component.html',
  styleUrls: ['./rudraksha.component.scss']
})
export class RudrakshaComponent implements OnInit {
  StatusCodes = StatusCodes;
  public m_rudrakshForm: FormGroup;
  public m_status: StatusCodes;
  private m_formSubmitted: boolean;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private backend: BackendService,
    private spinner: NgxSpinnerService,
    private toast: ToastService) {
    let alphaPattern = "^[A-zÀ-ž-.' ]*$";
    let alphaNumbericPattern = "^[A-zÀ-ž-.,'() 0-9_]*$"
    this.m_rudrakshForm = this.formBuilder.group({
      reg_id: [null],
      first_name: [null, [Validators.required, Validators.pattern(alphaPattern)]],
      last_name: [null, [Validators.required, Validators.pattern(alphaPattern)]],
      email: [null, [Validators.required, Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      // 
      country: [null, Validators.required],
      street: [null, [Validators.required, Validators.pattern(alphaNumbericPattern)]],
      house_number: [null, [Validators.required, Validators.pattern(alphaNumbericPattern)]],
      city: [null, [Validators.required, Validators.pattern(alphaNumbericPattern)]],
      postcode: [null, [Validators.required, Validators.pattern(alphaNumbericPattern)]],
    });
    this.m_formSubmitted = false;
  }

  ngOnInit(): void {
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.style.visibility = "hidden";
    this.m_formSubmitted = false;

    const id = this.route.snapshot.queryParamMap.get("id");
    const email = this.route.snapshot.queryParamMap.get("email");

    this.spinner.show();
    this.backend.getRudrakshaRecord(id, email).subscribe(
      (res: RudrakshaRecord) => {
        this.m_status = StatusCodes.FOUND;
        this.m_rudrakshForm.setValue(res);
      },
      (err: HttpErrorResponse) => {
        this.m_status = err.status;
        this.spinner.hide();
        this.toast.toastError(err.error.message);
      },
      () => {
        this.spinner.hide();
      }
    );
  }

  public submitForm() {
    this.m_formSubmitted = true;
    if (this.m_rudrakshForm.valid) {
      this.spinner.show();
      const rd_obj: RudrakshaRecord = Object.assign({}, this.m_rudrakshForm.value);
      this.backend.updateRudrakshaRecord(rd_obj).subscribe(
        (res) => {
          this.m_status = StatusCodes.OK;
          this.toast.toastSuccess("Data updated successfully");
        },
        (err: HttpErrorResponse) => {
          this.m_status = err.status;
          this.spinner.hide();
          this.toast.toastError(err.error.message);
        },
        () => {
          this.spinner.hide();
        }
      );
    }
    else {
      this.toast.toastError("Invalid Data. Please make sure, you fill in all the details correctly")
    }
  }

  public isFieldValid(field: AbstractControl) {
    return field.valid && field.touched;
  }

  public isFieldInValid(field: AbstractControl) {
    return field.invalid && (field.dirty || field.touched || this.m_formSubmitted);
  }

  get formControls() { return this.m_rudrakshForm.controls; }

}
