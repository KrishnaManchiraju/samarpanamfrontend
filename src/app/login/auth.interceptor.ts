import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, switchMap, filter, take } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { AuthResponse } from '../models/responses.model';
import { ToastService } from '../toasts/toasts.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private isRefreshing = false;
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(public authService: AuthService,
        private toast: ToastService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.authService.isLoggedIn && this.authService.isRefreshTokenExpired) {
            this.authService.logout();
            this.toast.toastError("Your session has expired. Please login again");

            return null;
        }

        if (this.authService.accessToken) {
            request = this.addToken(request, this.authService.accessToken);
        }

        return next.handle(request).pipe(catchError(error => {
            if (error instanceof HttpErrorResponse && error.status === 401
                && this.authService.isLoggedIn
                && this.authService.isAccessTokenExpired) {
                return this.handle401Error(request, next);
            } else {
                return throwError(error);
            }
        }));
    }

    private addToken(request: HttpRequest<any>, token: string) {
        return request.clone({
            setHeaders: {
                Authorization: 'JWT ' + token
            }
        });
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
        if (!this.isRefreshing) {
            this.isRefreshing = true;
            this.refreshTokenSubject.next(null);

            return this.authService.refreshTokens().pipe(
                switchMap((token: AuthResponse) => {
                    this.isRefreshing = false;
                    this.refreshTokenSubject.next(token.access);
                    return next.handle(this.addToken(request, token.access));
                }));

        } else {
            return this.refreshTokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(accessToken => {
                    return next.handle(this.addToken(request, accessToken));
                }));
        }
    }
}