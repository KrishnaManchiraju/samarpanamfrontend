import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

import jwt_decode from "jwt-decode";

import { environment } from '../../environments/environment';
import { AuthResponse } from '../models/responses.model';
import { LoginCredentials, RegistrationDetails } from '../models/input.models';


@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private rootUrl: string;

    constructor(private http: HttpClient, private router: Router) {
        if (environment.production) {
            this.rootUrl = '';
        } else {
            this.rootUrl = 'http://127.0.0.1:8000/';
        }

    }

    public loginUser(loginDetails: LoginCredentials) {
        const endPoint = 'auth/token/';
        const params = new HttpParams()
            .set('username', loginDetails.email)
            .set('password', loginDetails.password);

        return this.http.post(this.rootUrl + endPoint, params).pipe(tap((tokens: AuthResponse) => {
            this.setTokens(tokens.access, tokens.refresh);
        }));
    }

    public registerUser(regDetails: RegistrationDetails) {
        const endPoint = 'auth/register/';
        const params = new HttpParams()
            .set('email', regDetails.email)
            .set('password', regDetails.password)
            .set('first_name', regDetails.firstName)
            .set('last_name', regDetails.lastName);

        return this.http.post(this.rootUrl + endPoint, params);
    }

    public activateAccount(email: string, code: string) {
        const endPoint = 'auth/activate/';
        const params = new HttpParams()
            .set('email', email)
            .set('code', code);

        return this.http.put(this.rootUrl + endPoint, params).pipe(tap((tokens: AuthResponse) => {
            this.setTokens(tokens.access, tokens.refresh);
        }));
    }

    public signInWithGoogle(accessKey: string) {
        const endPoint = 'auth/signin_with_google/';
        const params = new HttpParams()
            .set('access_token', accessKey);

        return this.http.post(this.rootUrl + endPoint, params).pipe(tap((tokens: AuthResponse) => {
            this.setTokens(tokens.access, tokens.refresh);
        }));
    }


    public refreshTokens() {
        const refreshToken = localStorage.getItem('refresh_token');
        const data = { refresh: refreshToken };

        return this.http.post<AuthResponse>(this.rootUrl + 'auth/token/refresh/', data).pipe(tap((tokens: AuthResponse) => {
            this.setTokens(tokens.access, tokens.refresh);
        }));
    }

    private setTokens(access: string, refresh: string) {
        localStorage.setItem('access_token', access);
        localStorage.setItem('refresh_token', refresh);
        this.decodeToken(access, refresh);
    }

    private decodeToken(access: string, refresh: string) {
        try {
            const decodedAccessToken = jwt_decode(access) as any;
            const decodedRefreshToken = jwt_decode(refresh) as any;

            localStorage.setItem('user_id', decodedAccessToken.user_id);
            localStorage.setItem('access_expiry', decodedAccessToken.exp);
            localStorage.setItem('refresh_expiry', decodedRefreshToken.exp);

            if (decodedAccessToken.is_admin) {
                localStorage.setItem('is_admin', decodedAccessToken.is_admin);
            }
            if (decodedAccessToken.user) {
                localStorage.setItem('user', decodedAccessToken.user);
            }
        }
        catch (Error) {
            return null;
        }
    }

    private getAccessExpiry(): Date {
        const expiration = localStorage.getItem('access_expiry');

        return new Date(+expiration * 1000);
    }

    private getRefreshExpiry(): Date {
        const expiration = localStorage.getItem('refresh_expiry');

        return new Date(+expiration * 1000);
    }

    get isAccessTokenExpired(): boolean {
        return (new Date() >= this.getAccessExpiry());
    }

    get isRefreshTokenExpired(): boolean {
        return (new Date() >= this.getRefreshExpiry());
    }

    public logout() {
        localStorage.clear();
        this.router.navigateByUrl('login');
    }

    get isAdmin(): boolean {
        return !!localStorage.getItem('is_admin');
    }

    get isLoggedIn(): boolean {
        return !!this.accessToken;
    }

    get accessHeader(): string {
        return 'JWT '.concat(localStorage.getItem('access_token'));
    }

    get accessToken(): string {
        return localStorage.getItem('access_token');
    }

    get user(): string { return localStorage.getItem('user'); }

    get userId(): string { return localStorage.getItem('user_id'); }

}