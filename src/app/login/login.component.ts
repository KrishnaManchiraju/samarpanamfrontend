import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '../toasts/toasts.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginCredentials, RegistrationDetails } from '../models/input.models';
import { AuthService } from './auth.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    private googleApiSetup: boolean;
    private authInstance: any;
    private closeResult: string;
    private authUser: any;
    private returnUrl: string;

    public m_loginForm: FormGroup;
    public m_registrationForm: FormGroup;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private auth: AuthService,
        private modalService: NgbModal,
        private ngZone: NgZone,
        private spinner: NgxSpinnerService,
        public formBuilder: FormBuilder,
        public toastService: ToastService,) {

        this.m_loginForm = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, Validators.required]
        });

        this.m_registrationForm = this.formBuilder.group({
            firstName: [null, Validators.required],
            lastName: [null, Validators.required],
            email: [null, [Validators.required, Validators.email]],
            password: [null, Validators.required],
            passwordAgain: [null, Validators.required]
        })
    }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('full-screen');
        body.classList.add('login');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
        if (navbar.classList.contains('nav-up')) {
            navbar.classList.remove('nav-up');
        }
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/landing';
    }
    ngOnDestroy() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('full-screen');
        body.classList.remove('login');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    onLogin() {
        if (this.m_loginForm.valid) {
            const loginDetails = this.m_loginForm.value as LoginCredentials;
            this.spinner.show();

            this.auth.loginUser(loginDetails).subscribe(
                (res: any) => {
                    this.router.navigateByUrl(this.returnUrl);
                    this.toastService.toastSuccess("Logged in Successfully");
                },
                (err: HttpErrorResponse) => {
                    this.spinner.hide();
                    this.toastService.toastError(err.message);
                },
                () => {
                    this.spinner.hide();
                }
            );
        }
        else {
            this.toastService.toastError("Invalid/Incomplete Credentials");
        }
    }

    onRegister() {
        if (this.m_registrationForm.valid) {
            const regDetails = this.m_registrationForm.value as RegistrationDetails;
            this.spinner.show();

            this.auth.registerUser(regDetails).subscribe(
                (res: any) => {
                    this.toastService
                        .toastSuccess(
                            "Registered Successfully. An email has been sent to you to confirm your email address");
                },
                (err: HttpErrorResponse) => {
                    const errMsg = (err.error && err.error.message) ? err.error.message : err.message;
                    this.spinner.hide();
                    this.toastService.toastError(errMsg);
                },
                () => {
                    this.spinner.hide();
                    this.modalService.dismissAll();
                }
            );
        }
        else {
            this.toastService.toastError("Invalid/Incomplete Credentials");
        }
    }

    private async initGoogleAuth(): Promise<void> {
        //  Create a new Promise where the resolve 
        // function is the callback passed to gapi.load
        const pload = new Promise((resolve) => {
            gapi.load('auth2', resolve);
        });

        // When the first promise resolves, it means we have gapi
        // loaded and that we can call gapi.init
        return pload.then(async () => {
            await gapi.auth2
                .init({
                    client_id: '1086560242120-puuu9crdi4ea009g4c41l8mbbqibe6uo.apps.googleusercontent.com',
                    scope: 'profile'
                })
                .then(auth => {
                    this.googleApiSetup = true;
                    this.authInstance = auth;
                });
        });
    }

    public async onGoogleSignIn(): Promise<gapi.auth2.GoogleUser> {
        // Initialize gapi if not done yet
        if (!this.googleApiSetup) {
            await this.initGoogleAuth();
        }

        // Resolve or reject signin Promise
        return new Promise(async () => {
            await this.authInstance.signIn().then(
                user => {
                    this.authUser = user;
                    this.auth.signInWithGoogle(this.authUser.getAuthResponse().access_token).subscribe(
                        (res: any) => {
                            this.ngZone.run(
                                () => this.router.navigateByUrl(this.returnUrl)
                            );
                            this.toastService.toastSuccess("Logged in Successfully");
                        },
                        (err) => {
                            this.toastService.toastError("Cannot authenticate using Google")
                        },
                        () => { }
                    );
                },
                error => { },
                () => { }
            );
        });
    }

    public open(content) {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

}
