import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { BackendService } from '../backend.service';
import { AuthService } from '../login/auth.service';
import { DetailedEmailRequestResponse, Review, Status, EmailProviderChoicesResponse, EmailStatsResponse } from '../models/responses.model';
import { ToastService } from '../toasts/toasts.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmailRequestCompletion } from '../models/input.models';
import {NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-req-detail',
    templateUrl: './req-detail.component.html',
    styleUrls: ['./req-detail.component.scss']
})
export class ReqDetailComponent implements OnInit {    
    public requestDetails: DetailedEmailRequestResponse;
    public isAdmin: boolean;
    public m_requestCompletionForm: FormGroup;
    public m_emailProviders: EmailProviderChoicesResponse[];
    public m_emailStats: EmailStatsResponse;
    public collapsed: boolean = true;

    constructor(
        private route: ActivatedRoute,
        private auth: AuthService,
        private backend: BackendService,
        public toastService: ToastService,
        private spinner: NgxSpinnerService,
        private router: Router,
        private modalService: NgbModal,
        public formBuilder: FormBuilder,) {
            this.backend.getEmailProviders().subscribe(
                (res: EmailProviderChoicesResponse[]) => {  
                    this.m_emailProviders = res;
                },
                (err: any) => { },
                () => { }
            );                

            this.m_requestCompletionForm = this.formBuilder.group({
                email_provider: [null],
                email_provider_request_ref: [null]
            }); 
        }

    ngOnInit(): void {
        this.spinner.show();
        const id = this.route.snapshot.paramMap.get('id');
        this.backend.getDetailedRequest(id).subscribe(
            (res: DetailedEmailRequestResponse) => {
                this.requestDetails = res;
            },
            (err: HttpErrorResponse) => {
                this.spinner.hide();
                this.toastService.toastError(err.error.message);
                this.router.navigateByUrl('/landing');
            },
            () => {
                this.spinner.hide();
            }
        );

        this.isAdmin = this.auth.isAdmin;
    }

    public getStatusClass(list_step: number): string {
        return (list_step >= this.requestDetails.status.step ? "active" : "")
    }

    public getApprovalIcon(isApproved: Boolean): string {
        return (isApproved ? "fa fa-check text-success" : "fa fa-times text-danger");
    }

    public isCurrentUserPendingReview(review: Review): boolean {
        if (review.approved || this.requestDetails.status.step != 4) {
        return false;
        }
        const currentUser = +this.auth.userId;
        return currentUser == review.reviewer.id;
    }

    public approveRequest(review: Review) {
        this.spinner.show();
        this.backend.approveRequestReview(review.id).subscribe(
        (res) => {
            this.toastService.toastSuccess("Review Approved");
            review.approved = true;
        },
        (err) => {
            this.spinner.hide();
            this.toastService.toastError(err.message);
        },
        () => {
            this.spinner.hide();
        }
        );
    }

    public changeStep(step: number) {
        this.spinner.show();
        this.backend.updateRequestStatus(this.requestDetails.id, step).subscribe(
        (res: Status) => {
            this.toastService.toastSuccess("Status Updated");
            this.requestDetails.status = res;
        },
        (err) => {
            this.spinner.hide();
            this.toastService.toastError(err.message);
        },
        () => {
            this.spinner.hide();
        }
        );
    }

    public open(content) {
        this.modalService.open(content).result.then((result) => {            
        }, (reason) => {
        });
    }

    public completeRequest() {
        if (this.m_requestCompletionForm.valid) {
            const completionDetails = this.m_requestCompletionForm.value as EmailRequestCompletion;
            completionDetails.id = this.requestDetails.id.toString();
            this.spinner.show();
            this.backend.completeRequest(completionDetails).subscribe(
                (res: DetailedEmailRequestResponse) => {
                    this.toastService.toastSuccess("Marked as complete");
                    this.requestDetails.status = res.status;
                    this.requestDetails.email_provider = res.email_provider;
                    this.requestDetails.email_provider_desc = res.email_provider_desc;
                    this.requestDetails.email_provider_request_ref = res.email_provider_request_ref;
                },
                (err: HttpErrorResponse) => {
                    const errMsg = (err.error && err.error.message) ? err.error.message : err.message;
                    this.spinner.hide();
                    this.toastService.toastError(errMsg);
                },
                () => {
                    this.spinner.hide();
                    this.modalService.dismissAll();
                }
            );
        };
    }

    public beforeAccordionChange($event: NgbPanelChangeEvent) {
        //call api when panel is expanded and stats not already present
        if ($event.panelId === 'panel-1' && $event.nextState === true && !this.m_emailStats) {
            this.getEmailStats();
        };    
        this.collapsed =  !$event.nextState;
    }

    public getEmailStats() {
        this.spinner.show();
        this.backend.getEmailStats(this.requestDetails.id).subscribe(
            (res: EmailStatsResponse) => {
                this.m_emailStats = res;
            },
            (err) => {
                this.spinner.hide();
                this.toastService.toastError(err.message);
            },
            () => {
                this.spinner.hide();
            }
        );
    }
}